"""Evaluation for different feature detection|description|matching algorithms"""
import sys
from time import time

from typing import Tuple, List

import cv2.cv2 as cv
import numpy as np

from _preset import Colors, detectors, extractors, matchers, transformations


colors = Colors('bgr')


# This helps to find out, what exactly happened in opencv
def get_error_code(exc_info):
    string = exc_info[1].args[0]
    index = string.index('error: (') + len('error: (')
    rest = string[index:]
    next_index = rest.index(':')
    return int(rest[:next_index])


def get_error_description(code: int):
    return code, {-4: 'Out of Memory',
                  -210: 'Incompatible combination (matcher?)',     # probably - matcher
                  -215: 'Incompatible combination (extractor?)',     # probably detector/extractor pair
                  }[code]


def time_and_measure(det: cv.Feature2D,
                     ext: cv.Feature2D,
                     mat: cv.DescriptorMatcher,
                     images: Tuple[np.ndarray, np.ndarray]):
    """
    Collects information about given detector/extractor/matcher combination
    """
    result = {'dem': (det, ext, mat)}
    img1, img2 = images

    try:

        # detect key points

        st1 = time()
        kp1 = det.detect(img1)
        fn1 = time()

        st2 = time()
        kp2 = det.detect(img2)
        fn2 = time()

        result['detection_time'] = fn1 - st1, fn2 - st2
        result['keypoint_count'] = len(kp1), len(kp2)

        # draw key points

        result['marked_images'] = \
            cv.drawKeypoints(img1, kp1, None, color=colors.green), \
            cv.drawKeypoints(img2, kp2, None, color=colors.blue)

        # extract descriptors

        st1 = time()
        kp1, ds1 = ext.compute(img1, kp1)
        fn1 = time()

        st2 = time()
        kp2, ds2 = ext.compute(img2, kp2)
        fn2 = time()

        result['description_time'] = fn1 - st1, fn2 - st2

        # match descriptors

        st = time()
        matches = mat.knnMatch(ds1, ds2, k=2)  # k = 2 as we need best match and second best match
        fn = time()

        result['matching_time'] = fn - st
        result['matches_count'] = len(matches)

        # draw matches

        good_matches = [m for m, n in matches if m.distance < .75 * n.distance]     # Lowe's ratio test
        # this basically select best matches, if it's more than 25% better than second best

        result['good_matches_count'] = len(good_matches)
        result['good_ratio'] = len(good_matches) / len(matches)

        good_matches = sorted(good_matches, key=lambda m: m.distance)[0:100]    # No more than 100

        result['matched_images'] = cv.drawMatches(img1, kp1, img2, kp2, good_matches, None, singlePointColor=colors.red)

        # calculate total time

        result['total_time'] = sum([*result['detection_time'], *result['description_time'], result['matching_time']])

    except cv.error:
        result['error'] = get_error_description(get_error_code(sys.exc_info()))

    except:
        result['error'] = sys.exc_info()[0:2]

    return result


def get_statistics(results: dict, img: Tuple[np.ndarray, np.ndarray], *,
                   max_iter=None):
    """
    Collects information for all possible combinations of detectors,
    extractors and matchers

    :param results: empty (preferably) dict to store results of evaluation
    :param img: pair of image to execute algorithms on
    :param max_iter: - maximum number of iterations

    This function is meant to be used as iterator,
    with yielded value as progress marker
    When iterator is exhausted, calculations are finished
    and stored fully in 'results' dict
    Although, iteration can be stopped on any step

    max_iter will cause iterator to exhaust
    when specified iteration number reached
    """

    i = 1
    for d in detectors:
        for e in extractors:
            for m in matchers:
                results[f'{d} {e}-{m}'] = time_and_measure(detectors[d], extractors[e], matchers[m], img)

                if max_iter and i >= max_iter:
                    return

                yield i
                i += 1


def total_statistics() -> int:
    """Returns total amount of possible combinations"""
    return len(detectors) * len(extractors) * len(matchers)


def get_scores(ref: np.ndarray, img: np.ndarray,
               dem: Tuple[cv.Feature2D,  cv.Feature2D, cv.DescriptorMatcher]) \
               -> Tuple[float, float]:
    """
    Calculates repeatability score and matching score for given combination
    """
    repeatability, score = 0, 0

    try:
        detector, extractor, matcher = dem

        ref_kp = detector.detect(ref)
        img_kp = detector.detect(img)

        ref_kp, ref_des = extractor.compute(ref, ref_kp)
        img_kp, img_des = extractor.compute(ref, img_kp)

        matches = matcher.knnMatch(ref_des, img_des, 2)
        good_matches = [m for m, n in matches if m.distance < .75 * n.distance]

        repeatability = len(good_matches) / min(len(ref_kp), len(img_kp))
        score = len(good_matches) / len(matches)

    except Exception:
        pass    # traceback.print_exc()

    finally:
        return repeatability, score


def get_metrics(results: dict, ref: np.ndarray, transforms: List[str],
                dem: Tuple[cv.Feature2D,  cv.Feature2D, cv.DescriptorMatcher]):

    """
    Calculate metrics for given combination using
    given transformations on given image

    :param results: dictionary for results
    :param ref: source image
    :param transforms: list of transformation names
    :param dem: descriptor-extractor-matcher combination
    """

    i = 1
    for name in transforms:
        function, _from, _to, _step = transformations[name].values()

        score_list = []

        for value in np.arange(_from, _to, _step):
            img = function(ref, value)

            r, s = get_scores(ref, img, dem)

            if r + s != 0:
                score_list.append({'ratio': value,
                                   'repeatability': r,
                                   'matching_score': s,
                                   })

            yield i
            i += 1

        results[name] = score_list


def total_metrics(transforms: List[str]) -> int:
    count = 0

    for name in transforms:
        function, *args = transformations[name].values()

        count += len(np.arange(*args))

    return count
