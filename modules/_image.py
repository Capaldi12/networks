"""Utility functions to work with images"""
import math
import os

from typing import Tuple, Union

from matplotlib import pyplot as plt
import cv2.cv2 as cv
import numpy as np


img_dir = '/content/repo/images'


def list():
    """Lists all files in 'images' folder"""

    _, _, filenames = next(os.walk(img_dir))

    return filenames


def read(name: str) -> np.ndarray:
    """Reads image with given name from 'image' folder"""

    path = os.path.join(img_dir, name)

    if not os.path.exists(path):
        raise FileNotFoundError(f'Image "{path}" does not exists')

    return cv.imread(path)


def write(name: str, img: np.ndarray):
    """Saves given image to the file with given name in 'image' folder"""

    path = os.path.join(img_dir, name)

    return cv.imwrite(path, img)


def pairs_count():
    return len(os.listdir(os.path.join(img_dir, 'pairs')))


def load_pair(ind: int):
    """Loads image pair with given index"""

    pair_dir = os.path.join('pairs', str(ind))

    return read(os.path.join(pair_dir, '0.jpg')), read(os.path.join(pair_dir, '1.jpg'))


def display(img: np.ndarray, *, size=(10, 10), interpolation='bicubic', bgr=True):
    """Displays image using matplotlib"""
    if bgr:
        img = cv.cvtColor(img, cv.COLOR_BGR2RGB)

    fig, ax = plt.subplots(figsize=size, dpi=100)

    ax.imshow(img, interpolation=interpolation)
    ax.axis('off')


def display_fit(img: np.ndarray, *, bgr=True, dpi=100):
    """
    Displays image using matplotlib,
    trying to scale plot to fit with the image
    """
    if bgr:
        img = cv.cvtColor(img, cv.COLOR_BGR2RGB)

    w, h, _ = img.shape
    w /= dpi
    h /= dpi

    fig, ax = plt.subplots(figsize=(h, w), dpi=dpi)

    ax.imshow(img)
    ax.axis('off')
    plt.tight_layout()


def display_pair(pair: Tuple[np.ndarray, np.ndarray], *, size=(15, 10),
                 interpolation='bicubic', bgr=True):
    """Displays image pair"""
    o, l = pair

    if bgr:
        o = cv.cvtColor(o, cv.COLOR_BGR2RGB)
        l = cv.cvtColor(l, cv.COLOR_BGR2RGB)

    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=size, dpi=100)

    ax0.imshow(o, interpolation=interpolation)
    ax0.axis('off')

    ax1.imshow(l, interpolation=interpolation)
    ax1.axis('off')

    fig.subplots_adjust(wspace=0.01)


def crop_to_square(image: np.ndarray, centered=False):
    """Crops image to have square shape"""
    w, h, _ = image.shape
    least = min(w, h)

    if centered:
        w //= 2
        h //= 2
        least //= 2

        return image[w-least:w+least, h-least:h+least, :]
    else:
        return image[:least, :least, :]


def rescale(image: np.ndarray, factor: float = 1.):
    """Rescales image with given factor"""

    if float(factor) == 1.:    # 1 to 1 ratio don't need resize
        return image
    shape = tuple((np.array(image.shape[0:2]) * factor).astype(np.int32))

    # Upscale use Cubic, downscale use Area
    inter = cv.INTER_CUBIC if factor > 1 else cv.INTER_AREA

    return cv.resize(image, shape, interpolation=inter)


def rotate(image: np.ndarray, angle: Union[int, float], *, rad=False):
    """Rotates image to given angle(in degree unless 'rad' is True)"""

    if rad:     # opencv uses degree angles
        angle *= 180 / math.pi

    h, w, c = image.shape
    cx, cy = w // 2, h // 2

    M = cv.getRotationMatrix2D((cx, cy), -angle, 1)
    cos, sin = np.abs(M[0, 0]), np.abs(M[0, 1])

    nw = int(h * sin + w * cos)
    nh = int(h * cos + w * sin)

    M[0, 2] += nw / 2 - cx
    M[1, 2] += nh / 2 - cy

    return cv.warpAffine(image, M, (nw, nh))


def noise(image: np.ndarray, ratio: float = .5):
    """Applies random noise to image"""

    noise_up = np.random.randint(0, 256, image.shape, image.dtype)
    noise_down = np.random.randint(0, 256, image.shape, image.dtype)

    noise_total = (noise_up + noise_down) * ratio

    return cv.add(image, noise_total.astype(image.dtype))


def brightness(image: np.ndarray, ratio: float = .5):
    """Applies brightness to image"""

    bright = np.ones_like(image, dtype=image.dtype) * 255 * abs(ratio)

    if ratio >= 0:
        image = cv.add(image, bright.astype(image.dtype))
    else:
        image = cv.subtract(image, bright.astype(image.dtype))

    return image


def contrast(image: np.ndarray, ratio: float = 1.5):
    """Applies contrast to image"""

    contr = np.ones_like(image) * ratio

    return cv.multiply(image, contr, dtype=cv.CV_8U)


def blur(image: np.ndarray, ratio: float = .5):
    """Applies blur to image"""

    d = int(min(image.shape[0:2]) * (ratio / 10))
    kernel = np.ones((d, d)) / d**2

    return cv.filter2D(image, -1, kernel)


def sharpness(image: np.ndarray, ratio: float = .5):
    """Applies sharpness to image"""

    blured = blur(image, ratio * 4)

    return cv.addWeighted(image, 1.5, blured, - 0.5, 0)
