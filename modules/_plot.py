"""Different plotting functions"""
from matplotlib import pyplot as plt
import seaborn as sns


def antibars(data, *, figsize=(10, 10), dpi=150, **kwargs):
    fig, (ax0, ax1) = plt.subplots(1, 2, sharey='all', figsize=figsize, dpi=dpi, **kwargs)

    sns.barplot(data=data, x='Total time', y='Algorithm', ax=ax0)
    sns.barplot(data=data, x='Good matches ratio', y='Algorithm', ax=ax1)

    ax0.set_xlabel('Total execution time, s'), ax0.set_ylabel(''), ax1.set_ylabel('')
    ax0.invert_xaxis()

    plt.tight_layout(), plt.show()


def antibars_styled(data, style, **kwargs):
    with plt.style.context(style):
        antibars(data, **kwargs)


def plot_metric(data, style, alg_name, attack_name, **kwargs):
    with plt.style.context(style):
        data.plot(y=['repeatability', 'matching_score'],
                  subplots=True,
                  title=f'{alg_name} | {attack_name} attack',
                  **kwargs)
