"""Different pre-made objects for later use"""

import cv2.cv2 as cv

import _image as image

# detectors / extractors
detectors = {}
extractors = {}

try:
    fast: cv.FastFeatureDetector = cv.FastFeatureDetector_create()
    detectors['FAST'] = fast
except:
    print('FAST is unavailable')

try:
    sift: cv.SIFT = cv.SIFT_create(1000)
    detectors['SIFT'] = sift
    extractors['SIFT'] = sift
except:
    print('SIFT is unavailable') 

try:
    orb: cv.ORB = cv.ORB_create(1000)
    detectors['ORB'] = orb
    extractors['ORB'] = orb
except:
    print("ORB is unavailable")

try:
    brisk: cv.BRISK = cv.BRISK_create()
    detectors['BRISK'] = brisk
    extractors['BRISK'] = brisk
except:
    print('BRISK is unavailable')

try:
    kaze: cv.KAZE = cv.KAZE_create()
    detectors['KAZE'] = kaze
    extractors['KAZE'] = kaze
except:
    print('KAZE is unavailable')

try:
    akaze: cv.AKAZE = cv.AKAZE_create()
    detectors['AKAZE'] = akaze
    extractors['AKAZE'] = akaze
except:
    print('AKAZE is unavailable')

# matchers | all values are from opencv docs
matchers = {}

try:
    bfm_linear = cv.BFMatcher(cv.NORM_L2)   # suits for SIFT and alike
    matchers['BF(L2)'] = bfm_linear
except:
    print('BruteForce matcher (Linear) is unavailable')

try:
    bfm_hamming = cv.BFMatcher(cv.NORM_HAMMING)  # suits for ORB and alike
    matchers['BF(Hamming)'] = bfm_hamming
except:
    print('BruteForce matcher (Hamming) is unavailable')

try:
    fbm_kdtree = cv.FlannBasedMatcher({'algorithm': 0, 'trees': 5}, {})  # suits for SIFT and alike
    matchers['FLANN(KDTREE)'] = fbm_kdtree
except:
    print('FLANN based matcher (KDTree) is unavailable')

try:
    fbm_lsh = cv.FlannBasedMatcher({'algorithm': 6, 'table_number': 6, 'key_size': 12, 'multi_probe_level': 2}, {})  # suits for ORB and alike
    matchers['FLANN(LSH)'] = fbm_lsh
except:
    print('FLANN based matcher (LSH) is unavailable')


# Color tuples
class Colors:
    def __init__(self, scheme='bgr'):
        if scheme == 'bgr':
            self.red = (0, 0, 255)
            self.green = (0, 255, 0)
            self.blue = (255, 0, 0)
            self.white = (255, 255, 255)
            self.black = (0, 0, 0)
        elif scheme == 'rgb':
            self.red = (255, 0, 0)
            self.green = (0, 255, 0)
            self.blue = (0, 0, 255)
            self.white = (255, 255, 255)
            self.black = (0, 0, 0)
        elif scheme == 'hsv':
            self.red = (0, 100, 100)
            self.green = (120, 100, 100)
            self.blue = (240, 100, 100)
            self.white = (0, 0, 100)
            self.black = (0, 0, 0)
        else:
            raise ValueError(f"'{scheme}' is not valid scheme")


style = '/content/repo/modules/style.mplstyle'


# Image transformations
transformations = {'noise': {'function': image.noise,
                             'from': .1, 'to': 1.01, 'step': .05},
                   'brightness': {'function': image.brightness,
                                  'from': -.5, 'to': .51, 'step': .05},
                   'contrast': {'function': image.contrast,
                                'from': .2, 'to': 2.01, 'step': .05},
                   'sharpness': {'function': image.sharpness,
                                 'from': .1, 'to': 1.01, 'step': .05},
                   }
